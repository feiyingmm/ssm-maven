CREATE TABLE `t_user` (
                        `id` int(8) NOT NULL AUTO_INCREMENT,
                        `name` varchar(15) DEFAULT NULL,
                        `sex` tinyint(1) DEFAULT NULL,
                        `age` smallint(3) DEFAULT NULL,
                        `address` varchar(100) DEFAULT NULL,
                        `create_time` datetime DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;


CREATE TABLE `t_test` (
                        `id` int(8) NOT NULL AUTO_INCREMENT,
                        `color` varchar(10) DEFAULT NULL,
                        `len` int(10) DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
