package com.jack.spring;

import com.jack.common.listener.CarEvent;
import com.jack.controller.TestController;
import com.jack.controller.UserController;
import com.jack.entity.Car;
import com.jack.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * ${DESCRIPTION}
 *
 * @author mahonggeng
 * @create 2019-07-24 20:47
 **/
@Slf4j
public class FirstIoc {

    public static void main(String[] args) {
        factory2();
    }

    //编程式方式获取容器
    private static void factory1(){
        //获取Resource
        ClassPathResource resource = new ClassPathResource("spring/spring.xml");
        //创建BeanFactory
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        //创建一个载入BeanDefinition的读取器
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory);
        //从资源位置读入配置信息解析
        reader.loadBeanDefinitions(resource);
        TestController testController = factory.getBean(TestController.class);
        String hello = testController.hello();
        log.info(hello);
    }

    private static void factory2(){
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/spring.xml");
        /*TestController testController = applicationContext.getBean(TestController.class);
        String hello = testController.hello();
        log.info(hello);
        UserService userService = applicationContext.getBean(UserService.class);
        log.info("userService hello: {}", userService.hello());
        log.info("userService list: {}", userService.list());*/
        UserController userController = applicationContext.getBean(UserController.class);
        userController.save();
    }

    private static void factory3(){
//        FileSystemXmlApplicationContext applicationContext = new FileSystemXmlApplicationContext("F:\\workspace\\idea\\ssm-maven\\src\\main\\resources\\spring\\spring.xml");
        FileSystemXmlApplicationContext applicationContext = new FileSystemXmlApplicationContext("D:\\github\\ssm-maven\\src\\main\\resources\\spring\\spring.xml");

        TestController testController = applicationContext.getBean(TestController.class);
        String hello = testController.hello();
        log.info(hello);
    }

    private static void factory4(){
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring/spring2.xml");
        UserService bean = applicationContext.getBean(UserService.class);
        String hello = bean.hello();
        log.info(hello);
        //替换属性
        Car car = applicationContext.getBean(Car.class);
        log.info("car info: {}", car);
        //ApplicationListener事件
        CarEvent carEvent = new CarEvent("hello", "car hello");
        applicationContext.publishEvent(carEvent);
    }
}
