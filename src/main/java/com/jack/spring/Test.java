package com.jack.spring;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/31 11:26
 * @Version 1.0
 **/
public class Test {

    public static void main(String[] args) {
        Engineer java = new JavaEngineer();
        ManageHandler manageHandler = new ManageHandler(java);
        Engineer proxyInstance = (Engineer) Proxy.newProxyInstance(java.getClass().getClassLoader(), java.getClass().getInterfaces(), manageHandler);
        proxyInstance.coding();
    }
}

interface Engineer{
    public void coding();
}

class JavaEngineer implements Engineer{

    @Override
    public void coding() {
        System.out.println("coding ......");
    }
}

class ManageHandler implements InvocationHandler{

    private Object target;
    public ManageHandler(Object target){
        this.target = target;
    }

    private void pre(){
        System.out.println("coding pre");
    }

    private void after(){
        System.out.println("coding after");
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        pre();
        method.invoke(target, args);
        after();
        return null;
    }
}
