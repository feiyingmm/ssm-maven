package com.jack.spring;

import com.jack.dao.TestMapper;
import com.jack.entity.TestEntity;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * ${DESCRIPTION}
 *https://www.yiibai.com/mybatis/interface_comment.html
 * @author mahonggeng
 * @create 2019-08-05 21:41
 **/
public class MybatisTest {
    public static SqlSessionFactory sqlSessionFactory;
    public static String resource = "spring/mybatis-config.xml";
    static {
        try {
            InputStream inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream, "test");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            mapperTest(sqlSession);
        } finally {
            sqlSession.close();
        }
    }


    public static void sqlSessionTest(SqlSession sqlSession){
        List<Object> objects = sqlSession.selectList("com.jack.dao.TestMapper.getAll");
        System.out.println(objects);
    }

    public static void mapperTest(SqlSession sqlSession){
//        sqlSessionFactory.getConfiguration().addMapper(TestMapper.class);
        TestMapper mapper = sqlSession.getMapper(TestMapper.class);
        List<TestEntity> all = mapper.getAll();
        System.out.println(all);
//        sqlSession.clearCache();
//        mapper = sqlSession.getMapper(TestMapper.class);
//        all = mapper.getAll();
    }

    public void createSqlSessionFactory(){

    }
}
