package com.jack.dao;

import com.jack.entity.User;

import java.util.List;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/23 15:50
 * @Version 1.0
 **/
public interface UserMapper {

    public List<User> list();

    public Integer insert(User user);
}
