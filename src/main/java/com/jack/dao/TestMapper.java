package com.jack.dao;

import com.jack.entity.TestEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author mahonggeng
 * @create 2019-05-19 23:02
 **/
@Mapper
public interface TestMapper {
    public List<TestEntity> getAll();

    @Select({"select * from t_test"})
    public List<TestEntity> getAnnotation();

    public Object insert(TestEntity test);
}
