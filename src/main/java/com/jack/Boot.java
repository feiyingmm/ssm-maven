package com.jack;

import com.jack.dao.TestMapper;
import com.jack.entity.TestEntity;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Boot {
    public static void main(String[] args) throws IOException {
        String resource = "spring/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        //通过字节流读取了mybatis-config.xml文件，然后通过SqlSessionFactoryBuilder.build()方法，创建了一个SqlSessionFactory（这里用到了工厂模式和构建者模式）
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        TestMapper testMapper = sqlSession.getMapper(TestMapper.class);
        List<TestEntity> all = testMapper.getAll();
        System.out.println(all);
        sqlSession.commit();
        sqlSession.close();
    }
}
