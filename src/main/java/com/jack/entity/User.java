package com.jack.entity;


import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/23 15:43
 * @Version 1.0
 **/
@Data
@Builder
public class User {

    private Integer id;
    private String name;
    private Integer sex;
    private Integer age;
    private String address;
    private Date createTime;
}
