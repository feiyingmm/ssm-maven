package com.jack.entity;

import lombok.Data;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/8/6 17:02
 * @Version 1.0
 **/
@Data
public class Address {
    private Integer id;
    private String province;
    private String city;
    private String area;
}
