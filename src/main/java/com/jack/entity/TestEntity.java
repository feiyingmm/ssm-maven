package com.jack.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ${DESCRIPTION}
 *
 * @author mahonggeng
 * @create 2019-05-19 23:00
 **/
@Data
@Builder
public class TestEntity {
    private Integer id;
    private String color;
    private Integer len;
}
