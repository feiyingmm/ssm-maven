package com.jack.entity;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/30 9:15
 * @Version 1.0
 **/
public class Car {
    private String color;
    private String name;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
