package com.jack.common.exceptions;

import com.jack.common.result.ResultCodeEnum;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/3/28 16:32
 * @Version 1.0
 **/
public class BizException extends RuntimeException {
    private static final long serialVersionUID = -5875371379845226068L;

    protected String msg;
    protected int code;

    public BizException(int code, String msgFormat, Object... args) {
        super(String.format(msgFormat, args));
        this.code = code;
        this.msg = String.format(msgFormat, args);
    }

    public BizException() {
        super();
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(Throwable cause) {
        super(cause);
    }

    public BizException(String message) {
        super(message);
        this.code = -1;
        this.msg = message;
    }

    public BizException(int code, String message){
        super(message);
        this.code = code;
        this.msg = message;
    }

    public BizException(ResultCodeEnum code){
        super(code.getDesc());
        this.code = code.getCode();
        this.msg = code.getDesc();
    }
    public String getMsg() {
        return msg;
    }
    public int getCode() {
        return code;
    }
    /**
     * 实例化异常
     *
     * @param msgFormat
     * @param args
     * @return
     */
    public BizException newInstance(String msgFormat, Object... args) {
        return new BizException(this.code, msgFormat, args);
    }
}
