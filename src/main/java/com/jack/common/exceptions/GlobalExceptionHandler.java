package com.jack.common.exceptions;

import com.jack.common.result.CommonResult;
import com.jack.common.result.FailResult;
import com.jack.common.result.ResultCodeEnum;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import java.util.stream.Collectors;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/3/28 15:05
 * @Version 1.0
 **/
/*@Slf4j
@ControllerAdvice*/
public class GlobalExceptionHandler {

    /**
     * 处理实体类校验异常
     */
    /*@ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CommonResult bindExceptionHandler(BindException e, HttpServletRequest request, HttpServletResponse response) {
        String msg = collectErrorMsg(e.getBindingResult());
        if (StringUtils.isEmpty(msg))
            msg = ResultCodeEnum.FAIL.getDesc();
        return new FailResult(String.format("%s:%s", ResultCodeEnum.VALIDATEFAIL, msg));
    }

    @ExceptionHandler(BizException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CommonResult bizExceptionHandler(BizException e, HttpServletRequest request, HttpServletResponse response) {
//        log.error("业务异常BizException: code[{}], msg: {}", e.getCode(), e.getMsg());
        return new CommonResult(ResultCodeEnum.getByCode(e.getCode()), e.getMsg());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CommonResult unknowExceptionHandler(Exception e, HttpServletRequest request, HttpServletResponse response) {
//        log.error("服务器异常, status: {}, msg: {}", response.getStatus(), e);
        return new FailResult("服务器异常,请联系管理员");
    }

    private String collectErrorMsg(BindingResult bindingResult){
        return bindingResult.getFieldErrors()
                .stream()
                .map(fieldError -> "[" + fieldError.getField() + ":" + fieldError.getDefaultMessage() + "]")
                .collect(Collectors.joining(";"));
    }*/
}
