package com.jack.common.postprocess;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionVisitor;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.util.Set;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/30 8:34
 * @Version 1.0
 **/
public class CarBeanFactoryPostProcess implements BeanFactoryPostProcessor {

    private Set<String> obscenties;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
        for (String beanDefinitionName: beanDefinitionNames){
            BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanDefinitionName);
            BeanDefinitionVisitor beanDefinitionVisitor = new BeanDefinitionVisitor(strVal -> {
                if (isObscene(strVal))
                    return "******";
                return strVal;
            });
            beanDefinitionVisitor.visitBeanDefinition(beanDefinition);
        }
    }

    private boolean isObscene(Object value){
        String val = value.toString().toUpperCase();
        return this.obscenties.contains(val);
    }
    public void setObscenties(Set<String> obscenties) {
        this.obscenties = obscenties;
    }
}
