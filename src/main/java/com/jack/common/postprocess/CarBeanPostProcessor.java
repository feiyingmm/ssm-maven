package com.jack.common.postprocess;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/30 11:09
 * @Version 1.0
 **/
@Slf4j
public class CarBeanPostProcessor implements InstantiationAwareBeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        log.info("{}开始初始化, 时间: {}", beanName, System.currentTimeMillis());
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        log.info("{}初始化结束, 时间: {}", beanName, System.currentTimeMillis());
        return null;
    }

    @Override
    public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
        log.info("{}处理属性, 时间: {}", beanName, System.currentTimeMillis());
        return null;
    }
}
