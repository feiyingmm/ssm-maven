package com.jack.common.listener;

import org.springframework.context.ApplicationEvent;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/30 11:45
 * @Version 1.0
 **/
public class CarEvent extends ApplicationEvent {

    private String msg;

    public CarEvent(Object source) {
        super(source);
    }

    public CarEvent(Object source, String msg){
        super(source);
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
