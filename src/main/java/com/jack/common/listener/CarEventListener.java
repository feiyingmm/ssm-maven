package com.jack.common.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/30 11:48
 * @Version 1.0
 **/
@Slf4j
public class CarEventListener implements ApplicationListener {

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        log.info("发布的事件: {}", event.getSource());
        if (event instanceof CarEvent) {
            CarEvent carEvent = (CarEvent) event;
            log.info("source: {}, msg: {}", event.getSource(), ((CarEvent) event).getMsg());
        }
    }
}
