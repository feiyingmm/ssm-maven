package com.jack.common.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/30 14:17
 * @Version 1.0
 **/
//@Configuration
// 作用类似于<mvc:annotation-driven />, 注册springmvc的几大组件,开启mvc
@EnableWebMvc
// 作用类似于<context:component-scan base-package="com.jack.controller" />
// 自动扫描bean依赖注入, 注入注解处理类
@ComponentScan("com.jack")
// 类似于<mybatis:scan> mybatis的映射发现配置
@MapperScan(basePackages = "com.jack.dao", sqlSessionFactoryRef = "sqlSessionFactory")
// 类似于<tx:annotation-driven transaction-manager="transactionManager" />
// 启用注解事务
@EnableTransactionManagement(proxyTargetClass = true)
// 类似于 <aop:aspectj-autoproxy expose-proxy="true" proxy-target-class="true"/>
// 启用AOP功能
@EnableAspectJAutoProxy(exposeProxy = true, proxyTargetClass = true)
public class SpringConfig implements WebMvcConfigurer {
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    /**
     * 跨域问题解决
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }

    /**
     * 静态资源映射
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/img/**").addResourceLocations("file:/app/hyxy/integral/info/");
        registry.addResourceHandler("/files/**").addResourceLocations("classpath:templates/");
    }

    /**
     * Json数据转换
     * mvc:annotation-driven中配置的mvc:message-converters
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        /*Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder()
                .indentOutput(true)
                .dateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
        converters.add(new MappingJackson2XmlHttpMessageConverter(builder.createXmlMapper(true).build()));*/

        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        FastJsonConfig config = new FastJsonConfig();
        config.setSerializerFeatures(SerializerFeature.WriteMapNullValue);//保留空的字段
        //SerializerFeature.WriteNullStringAsEmpty,//String null -> ""
        //SerializerFeature.WriteNullNumberAsZero//Number null -> 0
        // 按需配置，更多参考FastJson文档哈

        converter.setFastJsonConfig(config);
        converter.setDefaultCharset(Charset.forName("UTF-8"));
        converter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
        converters.add(converter);
    }

    /**
     * 利用接口配置视图解析器
     */
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.jsp("/WEB-INF/jsp/", ".jsp");
    }

    //不用接口形式配置视图解析器
    @Bean
    InternalResourceViewResolver internalResourceViewResolver(){
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    /**
     * Spring内置的Mulitipart Request技术就包含了文件上传的处理, 其中MultipartResolver可以支持文件上传请求. 实现这个标准有两种方式:
     * 1. commons-fileupload, 现在普遍用的, 需要引入依赖, 名字一定叫做multipartResolver, CommonsMultipartRver
     * 2. 基于Servlet3.0 StandardServletMultipartResolveresolver
     */
    @Bean(value = "multipartResolver")
    public MultipartResolver multipartResolver(){
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        //内存中最大值
        multipartResolver.setMaxInMemorySize(40960);
        //上传文件大小限制为50M, 50*1024*1024
        multipartResolver.setMaxUploadSize(52428800);
        multipartResolver.setDefaultEncoding("UTF-8");
        return multipartResolver;
    }
}
