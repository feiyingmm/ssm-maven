package com.jack.common.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/10/11 15:16
 * @Version 1.0
 **/
//public class MyWebApplicationInitializer implements WebApplicationInitializer {
public class MyWebApplicationInitializer {
//    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        //加载Spring配置
        AnnotationConfigWebApplicationContext ac = new AnnotationConfigWebApplicationContext();
        ac.register(SpringConfig.class);
        //ac.setConfigLocation("classpath://spring/spring.xml");//加载xml配置
        ac.refresh();

        //注册DispatcherServlet
        DispatcherServlet servlet = new DispatcherServlet(ac);
        ServletRegistration.Dynamic registration = servletContext.addServlet("app", servlet);
        registration.setLoadOnStartup(1); //设置servlet在tomcat启动的时候执行init()
        registration.addMapping("/");

        MultipartConfigElement multipartConfigElement = new MultipartConfigElement("d:");
        registration.setMultipartConfig(multipartConfigElement);
    }
}
