package com.jack.common.result;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/3/29 14:23
 * @Version 1.0
 **/
public class FailResult extends CommonResult {
    public FailResult(){
        super(ResultCodeEnum.FAIL, "服务器错误", null);
    }

    public FailResult(String msg){
        super(ResultCodeEnum.FAIL, msg, null);
    }
}
