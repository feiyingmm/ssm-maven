package com.jack.common.result;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/3/29 14:18
 * @Version 1.0
 **/
public class SuccessResult extends CommonResult {

    public SuccessResult(){
        super(ResultCodeEnum.SUCCESS, "请求成功", "success");
    }

    public SuccessResult(String msg, Object data){
        super(ResultCodeEnum.SUCCESS, msg, data);
    }

    public SuccessResult(Object data){
        super(ResultCodeEnum.SUCCESS, ResultCodeEnum.SUCCESS.getDesc(), data);
    }
}
