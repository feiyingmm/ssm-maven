package com.jack.common.result;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/3/28 14:26
 * @Version 1.0
 **/
public enum ResultCodeEnum {
    SUCCESS(0, "请求成功"),
    FAIL(-1, "请求失败"),
    USER_NOT_EXISTED(1, "用户不存在或已被禁用"),
    PASSWORD_ERROR(2, "密码错误"),
    CAPTCHACODE_ERROR(3, "验证码错误"),
    CAPTCHACODE_INVALID(4, "验证码失效, 请刷新重试"),

    TOKEN_INVALID(5, "token失效, 请重新登录"),
    USER_DISABLE(6, "用户被禁用"),
    USER_HAS_EXISTS(7, "用户已存在"),
    USER_UNREGISTER(100, "用户未注册"),

    VALIDATEFAIL(5000, "校验失败"),
    REQ_PARAM_LACK(5001, "参数缺失或值为空"),
    REQ_PARAM_WRONG(5002, "参数值有误"),

    SCANNING_CLIENT_WRONG(5100, "请使用积分系统用户端扫描"),

    LIVE_CODE_ERROR(6000, "验证码朗读错误"),
    LIVE_DETECTION_ERROR(6001, "活体检测不通过"),
    LIVE_NOT_COMPLETE(6003, "活体检测未完成"),

    PERSON_VERIFY(7000, "身份验证不通过, 请核对输入信息是否正确"),

    SYSTEM_ERROR(8001, "系统异常");

    private int code;
    private String desc;

    ResultCodeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }
    public static ResultCodeEnum getByCode(int code){
        ResultCodeEnum[] values = ResultCodeEnum.values();
        for (ResultCodeEnum e:values){
            if (e.getCode()==code)
                return e;
        }
        return null;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
}
