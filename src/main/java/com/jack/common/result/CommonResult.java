package com.jack.common.result;

import java.beans.Transient;
import java.io.Serializable;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/3/28 14:19
 * @Version 1.0
 **/
public class CommonResult implements Serializable {

    private int code;
    private String msg;
    private Object data;

    public CommonResult(){}

    public CommonResult(ResultCodeEnum code){
        this.code = code.getCode();
        this.msg = code.getDesc();
        this.data = "";
    }

    public CommonResult(ResultCodeEnum code, String msg){
        this.code = code.getCode();
        this.msg = msg;
        this.data = "";
    }

    public CommonResult(ResultCodeEnum code, String msg, Object data){
        this.code = code.getCode();
        this.msg = msg;
        this.data = data;
    }

    @Transient
    public boolean success(){
        return code == ResultCodeEnum.SUCCESS.getCode();
    }

    @Transient
    public boolean fail(){
        return code == ResultCodeEnum.FAIL.getCode();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
