package com.jack.common.annotations;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/4/25 18:26
 * @Version 1.0
 **/
@Slf4j
@Aspect
@Component
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class TimeLogAspect {

    @Pointcut("@annotation(com.jack.common.annotations.TimeLog)")
    private void pointCut(){}

    @Around("pointCut()")
    public Object process(ProceedingJoinPoint pjp) throws Throwable {
        long beginTime = System.currentTimeMillis();
        log.info("-----方法[{}]开始执行, beginTime: {}", pjp.getSignature().getName(), beginTime);
        Object result = pjp.proceed();
        long endTime = System.currentTimeMillis();
        float times = (float)(endTime-beginTime)/1000f;
        //BigDecimal.valueOf(endTime-beginTime).divide(1000l, 4, RoundingMode.)
        log.info("-----方法[{}], 执行所用时间为: {} 秒, beginTime: {}, endTIme: {}", pjp.getSignature().getName(), times, beginTime, endTime);
        return result;
    }

    public static void main(String[] args) {
        long a = 1235l;
        float b = (float)a /1000;
        System.out.println(b);
        BigDecimal divide = BigDecimal.valueOf(a).divide(BigDecimal.valueOf(1000l), 4, RoundingMode.UP);
        System.out.println(divide.toString());
    }
}
