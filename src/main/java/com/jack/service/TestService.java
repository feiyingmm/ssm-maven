package com.jack.service;

import com.jack.entity.TestEntity;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author mahonggeng
 * @create 2019-05-19 23:12
 **/
public interface TestService {
    public List<TestEntity> selectTests();

    List<TestEntity> testAnnotation();

    void insert(TestEntity test);
}
