package com.jack.service;

import com.jack.entity.TestEntity;
import com.jack.entity.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/23 15:49
 * @Version 1.0
 **/
public interface UserService {
    String hello();

    Integer insert(User user);

    void saveAll(User user, TestEntity test);

    void saveTest(User user, TestEntity test);

    void priSave(User user, TestEntity test);

    List<User> list();
}
