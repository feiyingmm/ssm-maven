package com.jack.service;

import com.jack.common.annotations.TimeLog;
import com.jack.dao.TestMapper;
import com.jack.dao.UserMapper;
import com.jack.entity.TestEntity;
import com.jack.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/23 15:49
 * @Version 1.0
 **/
@Slf4j
@Service
public class UserServiceImpl implements UserService, BeanPostProcessor {

    @Resource
    private UserMapper userMapper;
    @Resource
    private TestMapper testMapper;
    @Autowired
    private TestService testService;

    @Autowired
    private ApplicationContext context;

    private UserService userService;

    /*@PostConstruct //初始化方法
    public void setSelf(){
        //此种方法不适合于prototype Bean，因为每次getBean返回一个新的Bean
        userService = context.getBean(UserService.class);
    }*/

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("-------------------postProcess调用");
        if (bean instanceof UserService)
            this.userService = (UserService) bean;
        return bean;
    }

    @Override
    public String hello(){
        return "hello";
    }

    @Transactional
    @Override
    public Integer insert(User user){
        user.setCreateTime(new Date());
        Integer id = userMapper.insert(user);
        System.out.println(id);
        return id;
    }

    @Transactional
    @Override
    public void saveAll(User user, TestEntity test){
        Integer userId = this.insert(user);
        int a = 5/0;
        testService.insert(test);
    }

    @Override
    public void saveTest(User user, TestEntity test){
        System.out.println("进入保存");
        logProxy(this, "this对象");
        logProxy((UserService)AopContext.currentProxy(), "AopContext.currentProxy()对象");

//        priSave(user, test);
        ((UserService)AopContext.currentProxy()).priSave(user, test);
//        userService.priSave(user, test);
    }

    private void logProxy(Object obj, String type){
        //这个不管是使用jdk还是cglib, 只要是代理对象就返回true
        boolean aopProxy = AopUtils.isAopProxy(obj);
        boolean cglibProxy = AopUtils.isCglibProxy(obj);
        boolean jdkDynamicProxy = AopUtils.isJdkDynamicProxy(obj);
        log.info("type: {}, aopProxy: {}, cglibProxy: {}, jdkDynamicProxy: {}", type, aopProxy, cglibProxy, jdkDynamicProxy);
    }

    @Transactional(rollbackFor = Exception.class)
    public void priSave(User user, TestEntity test){
        userMapper.insert(user);
        int a = 5/0;
        testMapper.insert(test);
    }

    @TimeLog
    @Override
    public List<User> list() {
        return userMapper.list();
    }

    public void setTestService(TestService testService) {
        this.testService = testService;
    }
}
