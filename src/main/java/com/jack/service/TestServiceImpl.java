package com.jack.service;

import com.jack.dao.TestMapper;
import com.jack.entity.TestEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author mahonggeng
 * @create 2019-05-19 23:13
 **/
@Service
public class TestServiceImpl implements TestService {

    @Resource
    TestMapper testMapper;

    private UserService userService;
    private String jackMsg;

    @Override
    public List<TestEntity> selectTests() {
        return testMapper.getAll();
    }

    @Override
    public List<TestEntity> testAnnotation(){
        return testMapper.getAnnotation();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insert(TestEntity test) {
        testMapper.insert(test);
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setJackMsg(String jackMsg) {
        this.jackMsg = jackMsg;
    }
}
