package com.jack.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/8/9 9:22
 * @Version 1.0
 **/
@Slf4j
@Controller
@SessionAttributes("articleId")
public class FollowMeController {

    private final String[] sensitiveWords = new String[]{"k1", "s2"};

    @ModelAttribute("comment")
    public String replaceSensitiveWords(String comment){
        if (comment!=null){
            log.info("原始comment: {}", comment);
            for (String sw:sensitiveWords)
                comment = comment.replaceAll(sw, "");
            log.info("去敏感词后comment: {}", comment);
        }
        return comment;
    }

    //http://localhost:8888/articles/67/comment?comment=好s2赞k1k1
    @RequestMapping("/articles/{articleId}/comment")
    public String doComment(@PathVariable String articleId, RedirectAttributes attributes, Model model){
        attributes.addFlashAttribute("comment", model.asMap().get("comment"));
        model.addAttribute("articleId", articleId);
        return "redirect:/showArticle";
    }

    @RequestMapping("/showArticle")
    public String showArticle(Model model, SessionStatus sessionStatus){
        String articleId = (String) model.asMap().get("articleId");
        model.addAttribute("articleTitle", articleId+"号文章标题");
        model.addAttribute("article", articleId+"号文章内容");
        sessionStatus.setComplete();
        return "article";
    }

    /*@RequestMapping("/")
    public String index(){
        return "index";
    }*/
}
