package com.jack.controller;

import com.jack.common.annotations.TimeLog;
import com.jack.common.result.CommonResult;
import com.jack.common.result.FailResult;
import com.jack.common.result.SuccessResult;
import com.jack.entity.TestEntity;
import com.jack.entity.User;
import com.jack.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;

/**
 * @Description: TODO
 * @Author mahonggeng
 * @create 2019/7/23 15:21
 * @Version 1.0
 **/
//@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @TimeLog
    @GetMapping("/list")
    public CommonResult list(){
        List<User> list = userService.list();
        return new SuccessResult(list);
    }

    @PostMapping("/insert")
    public CommonResult insert(@RequestBody User user){
        try {
            userService.insert(user);
        }catch (Exception e){

            return new FailResult();
        }
        return new SuccessResult();
    }

    @TimeLog
    @PostMapping("/save")
    public CommonResult save(){
        User user = User.builder().name("Jack")
                .age(18)
                .sex(1)
                .address("beijing")
                .createTime(new Date())
                .build();
        TestEntity test = TestEntity.builder()
                .color("red")
                .len(3)
                .build();

        try {
            userService.saveAll(user, test);
        } catch (Exception e){
            System.out.println(e.getMessage());
            return new FailResult();
        }
        return new SuccessResult();
    }

    @PostMapping("/testSave")
    public CommonResult testSave(){
        User user = User.builder().name("hello")
                .age(20)
                .sex(1)
                .address("beijing")
                .createTime(new Date())
                .build();
        TestEntity test = TestEntity.builder()
                .color("blue")
                .len(5)
                .build();
        try {
            userService.saveTest(user, test);
//            userService.priSave(user, test);
        } catch (Exception e){
            System.out.println(e.getMessage());
            return new FailResult("保存失败");
        }
        return new SuccessResult();
    }
}
