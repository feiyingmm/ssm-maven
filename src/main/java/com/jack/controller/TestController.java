package com.jack.controller;

import com.jack.common.annotations.TimeLog;
import com.jack.entity.TestEntity;
import com.jack.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author mahonggeng
 * @create 2019-05-19 23:11
 **/
@Controller
public class TestController {

    @Value("jack.msg")
    private String msg;

    @Autowired
    private TestService testService;

    @GetMapping("/selectTests")
    @ResponseBody
    public List<TestEntity> selectTests(){

        return testService.selectTests();
    }

    @ResponseBody
    @GetMapping("/annotation")
    public List<TestEntity> annotation(){
        return testService.testAnnotation();
    }

    @TimeLog
    @GetMapping("/hello")
    @ResponseBody
    public String hello(){
        return "hello world";
    }

}
